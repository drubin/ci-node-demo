FROM node:10.13-alpine
WORKDIR /app
ADD package.json package-lock.json /app/
RUN npm install --production
ADD . /app
CMD ["npm", "start"]
