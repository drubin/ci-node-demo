# CI node demo

Example project to show off ability for CI to be reuseable.  See [presenation](https://drubin.github.io/presentations/2018/ci-cd-meetup-berlin-accelerate-pipeline/#1) for full over view.

See [ci-base project](https://gitlab.com/drubin/ci-base) for more info

# Demo

`.gitlab-ci.yml`

```yaml
stages:
  - setup
  - test
  - release

include:
  - https://gitlab.com/drubin/ci-base/raw/master/includes/docker.yml
  - https://gitlab.com/drubin/ci-base/raw/master/includes/node.yml

image: node:10.13-alpine
````

### Master pipeline

![Node pipeline](images/node-master-pipeline.png)

